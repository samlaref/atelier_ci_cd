
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(subject = "test", message = "this is a debugging message"):
    try:
        # Email configuration
        sender_email = 'your_email@example.com'
        receiver_email = 'stobelemfr@gmail.com'

        # Create a multipart message
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = receiver_email
        msg['Subject'] = subject

        # Attach the message to the email
        msg.attach(MIMEText(message, 'plain'))

        # SMTP server configuration
        smtp_server = 'smtp.example.com'
        smtp_port = 587
        smtp_username = 'your_email@example.com'
        smtp_password = 'your_password'

        # Create a secure connection with the SMTP server
        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.starttls()
            server.login(smtp_username, smtp_password)
            server.send_message(msg)
            print('Email sent successfully')
        return True
    except Exception as e:
        print('An error occurred while sending the email:', str(e))
        return False
