import numpy as np 
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import spacy
import os
from sklearn.model_selection import train_test_split


def read_data():
    for dirname, _, filenames in os.walk('./input'):
        for filename in filenames:
            print(os.path.join(dirname, filename))

    df=pd.read_csv('./input/Emotion_classify_Data.csv')

    print(df['Emotion'].value_counts())
    return df

def preprocess_data(df): 

    def preprocess(text):
        doc = nlp(text)
        list = []
        for token in doc:
            if token.is_stop or token.is_punct:
                continue
            list.append(token.lemma_)
        
        return " ".join(list) 

    try:
        # remove duplicates 
        df.drop_duplicates(inplace=True, subset=['Comment'])
        df['sentiment']=df['Emotion'].map({
            'anger':0,
            'joy':1,
            'fear':2
        })

        nlp=spacy.load("en_core_web_sm")
        df['p_comment'] = df['Comment'].apply(preprocess) 
        return df
    
    except Exception as e:
        print(f"An error occurred: {str(e)}")


def train_test_model(df):

    from sklearn.pipeline import Pipeline
    from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.feature_extraction.text import TfidfVectorizer

    X_train,X_test,Y_train,Y_test=train_test_split(df['p_comment'],df['sentiment'],test_size=0.2)
    X_train.shape
    X_test.shape

    clf = Pipeline([
        ('vectorizer_tfidf',TfidfVectorizer()),      
        ('dec_tree', DecisionTreeClassifier())
    ])
    clf.fit(X_train, Y_train)
    prediction = clf.predict(X_test)
    print(classification_report(Y_test, prediction))
    accuracy = accuracy_score(Y_test, prediction)
    print(f"Accuracy: {accuracy}")

    c_m = confusion_matrix(Y_test, prediction)

    print(c_m)

    return accuracy, c_m

def plot_confusion_matrix(c_m):
    plt.figure(figsize=(10, 6))
    sns.heatmap(c_m, annot=True, fmt='d', cmap='plasma', linewidths=0.4, square=True, cbar=True)
    plt.xlabel('Predicted')
    plt.ylabel('Actual')
    plt.title('Confusion Matrix')
    plt.show()

    return