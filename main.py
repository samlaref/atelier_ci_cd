from send_email import send_email
from functions import read_data, preprocess_data, train_test_model, plot_confusion_matrix

if __name__ == "__main__":
    # Read the data
    df = read_data()
    df = preprocess_data(df)

    (accuracy, c_m) = train_test_model(df)
    print(f"Accuracy: {accuracy}")

    plot_confusion_matrix(c_m)
    # Call the function to send the email
    send_email("python script running", "The python script has finished running successfully!")
