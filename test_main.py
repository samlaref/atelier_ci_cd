import unittest
import os
import pandas as pd
from functions import read_data, preprocess_data, train_test_model


class TestDataReading(unittest.TestCase):

    df = None
    dataset = None

    @classmethod
    def setUpClass(cls):
        # Load or create your dataset once for all test functions
        cls.dataset = read_data()
        cls.df = preprocess_data(cls.dataset)

    def test_data_frame(self):
        # Test that the function returns a valid DataFrame
        self.assertIsInstance(self.dataset, pd.DataFrame)
        print(self.dataset.head())
        print(self.df.head())

    def test_emotion_counts(self):
        # test if sentiment column is created and only has 0,1,2 values
        expected_values = [0, 1, 2]
        assert (self.df['sentiment'].isin(expected_values)).all(), "Values in 'sentiment' column are not as expected"
        self.assertEqual(len(self.df), 5934) 
        # assert that column p_comment is created
        self.assertTrue('p_comment' in self.df.columns)
        # assert that, for each line, the length of the p_comment is greater than 0
        self.assertTrue((self.df['p_comment'].str.len() > 0).all())

    def test_train_test_model(self):
        (accuracy, c_m) = train_test_model(self.df)
        self.assertIsInstance(accuracy, float)
        # assert that the confusion matrix has the correct shape
        self.assertEqual(c_m.shape, (3, 3))
        # assert that the accuracy is between 0.6 and 1
        self.assertTrue(0.6 <= accuracy <= 1)

if __name__ == '__main__':
    unittest.main()